#include "main.h"
#include <stdint.h>
#include "stm32f412rx.h"



void config() {
	RCC->CR |= RCC_CR_HSEON;
	while (!(RCC->CR & RCC_CR_HSERDY)); //Wait for HSE to startup
	
	FLASH->ACR |= 2; //4 wait states for 144MHz operation
	
	RCC->PLLCFGR = (RCC->PLLCFGR & (~RCC_PLLCFGR_PLLM)) | RCC_PLLCFGR_PLLM_3; //Set to 16/8 = 2MHz into PLL
	RCC->PLLCFGR = (RCC->PLLCFGR & (~RCC_PLLCFGR_PLLN)) | (100 << 6); //Set to 200MHz PLL output clock
	RCC->PLLCFGR = (RCC->PLLCFGR & (~RCC_PLLCFGR_PLLP)); //PLLP = 0 for /2 clock to get 100MHz operating core clock
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC; //PLL source set to HSE
	
	RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_PPRE2)); //Set APB2 to /1 for 100MHz
	RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_PPRE1)) | (RCC_CFGR_PPRE1_2); //Set APB1 to /2 for 50MHz
	RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_HPRE)); //Set AHB to /1 for 100MHz
	
	RCC->CR |= RCC_CR_PLLON; //Enable the main PLL
	while (!(RCC->CR & RCC_CR_PLLRDY)); //Wait for the main PLL to startup
	
	RCC->CFGR = (RCC->CFGR & (~RCC_CFGR_SW)) | RCC_CFGR_SW_1; //Set to main clock derived from PLL output
	while (!((RCC->CFGR & RCC_CFGR_SWS_1) && (!(RCC->CFGR & RCC_CFGR_SWS_0)))); //Wait for the system clock to change
	
	SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  // set CP10 and CP11 Full Access to enable the FPU 
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	RCC->APB1ENR |= RCC_APB1ENR_CAN1EN;
	RCC->APB1ENR |= RCC_APB1ENR_CAN2EN;
}



void setupPin(GPIO_TypeDef * GPIOp, uint8_t pin, uint8_t pin_mode, uint8_t output_type, uint8_t speed, uint8_t pull_up_pull_down, uint8_t initial_value, uint8_t af) {
	GPIOp->ODR = (GPIOp->ODR & ~(1 << pin)) | (((uint32_t)initial_value) << pin); //Set the initial value
	
	if (pin < 8) {
		GPIOp->AFR[0] = (GPIOp->AFR[0] & ~(0xF << (pin * 4))) | (((uint32_t)af) << (pin * 4)); //Set the AF mode
	} else {
		GPIOp->AFR[1] = (GPIOp->AFR[1] & ~(0xF << ((pin - 8) * 4))) | (((uint32_t)af) << ((pin - 8) * 4)); //Set the AF mode
	}
	
	GPIOp->PUPDR = (GPIOp->PUPDR & ~(0x3 << (pin * 2))) | (((uint32_t)pull_up_pull_down) << (pin * 2)); //Set the pull up, pull down mode
	
	GPIOp->OSPEEDR = (GPIOp->OSPEEDR & ~(0x3 << (pin * 2))) | (((uint32_t)speed) << (pin * 2)); //Set output speed
	
	GPIOp->OTYPER = (GPIOp->OTYPER & ~(0x1 << (pin))) | (((uint32_t)output_type) << (pin)); //Set the output type
	
	GPIOp->MODER = (GPIOp->MODER & ~(0x3 << (pin * 2))) | (((uint32_t)pin_mode) << (pin * 2)); //Set the pin mode
}





int main() {
	//Setup GPIO
	setupPin(GPIOB, 5, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 9); //CAN2 RX
	setupPin(GPIOB, 6, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 9); //CAN2 TX
	setupPin(GPIOC, 3, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_SPD_LOW, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 0); //CAN2 STBY, init low to enable CAN
	
	setupPin(GPIOA, 11, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 9); //CAN1 RX
	setupPin(GPIOA, 12, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 9); //CAN1 TX
	setupPin(GPIOA, 8, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_SPD_LOW, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 0); //CAN1 STBY, init low to enable CAN
	
	setupPin(GPIOA, 13, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 0); //Debug
	setupPin(GPIOA, 14, GPIO_AF, GPIO_PUSH_PULL, GPIO_SPD_VERY_HIGH, GPIO_PUPD_NO, GPIO_INIT_VAL_LOW, 0); //Debug
	

	
	//Setup CAN1
	//CAN1 = Jeep
  CAN1->MCR |= CAN_MCR_INRQ;            //Set CAN to initialization mode
  CAN1->MCR &= ~CAN_MCR_SLEEP;          //Exit default sleep mode
  while (!(CAN1->MSR & CAN_MSR_INAK));  //Wait for CAN to enter initialization mode
  
	CAN1->MCR &= ~CAN_MCR_TXFP;           //Priority driven by identifier
	
  CAN1->BTR &= ~(0x037F0000);           //Reset default time segments to zero
  CAN1->BTR |= (10)<<16;               //Set time segment 1 quanta
  CAN1->BTR |= (7)<<20;               //Set time segment 2 quanta
  CAN1->BTR |= 0x4;                     //Set quanta for 10MHz tq on 50MHz APB1 with above segments
	CAN1->BTR |= CAN_BTR_SJW;
	
  
  CAN1->MCR &= ~CAN_MCR_TTCM;
  CAN1->MCR |= CAN_MCR_ABOM; //Allow for automatic recovery from a bus-off fault
  CAN1->MCR &= ~CAN_MCR_AWUM;
  CAN1->MCR &= ~CAN_MCR_NART; //Do retry, these messages must get through to the Jeep
  CAN1->MCR &= ~CAN_MCR_RFLM;
  CAN1->MCR &= ~CAN_MCR_TXFP;
  
  CAN1->MCR &= ~CAN_MCR_INRQ;           //Exit initialization mode
  
  while (CAN1->MSR & CAN_MSR_INAK);     //Wait for initialization mode to close
		
  CAN1->FMR |= CAN_FMR_FINIT;           //Enter filter initialization mode
	
	//CAN1 filter banks
	CAN1->sFilterRegister[0].FR1 = (0x0); //Command
  CAN1->sFilterRegister[0].FR2 = (0x0); //Accept ALL messages incoming
  CAN1->FS1R |= CAN_FS1R_FSC0;         //32 bit mode
	CAN1->FM1R &= ~CAN_FM1R_FBM0;         //Mask mode
	CAN1->FFA1R &= ~CAN_FFA1R_FFA0;       //Set to FIFO0
  CAN1->FA1R |= CAN_FA1R_FACT0;         //Enable filter 0
	
	//CAN2 filter banks (start at 14)
	CAN1->sFilterRegister[14].FR1 = (0x0); //Command
  CAN1->sFilterRegister[14].FR2 = (0x0); //Accept ALL messages incoming
  CAN1->FS1R |= CAN_FS1R_FSC14;         //32 bit mode
	CAN1->FM1R &= ~CAN_FM1R_FBM14;         //Mask mode
	CAN1->FFA1R &= ~CAN_FFA1R_FFA14;       //Set to FIFO0
  CAN1->FA1R |= CAN_FA1R_FACT14;         //Enable filter 0
	
	CAN1->FMR &= ~CAN_FMR_FINIT;          //Exit filter initialization mode
	

	
	//Setup CAN2 (Slave to CAN1, CAN2's filter banks configured to start at 
	//CAN1 filter bank 14, CAN2 has no filter banks in its own register space)
	//CAN2 = Sway Bar
  CAN2->MCR |= CAN_MCR_INRQ;            //Set CAN to initialization mode
  CAN2->MCR &= ~CAN_MCR_SLEEP;          //Exit default sleep mode
  while (!(CAN2->MSR & CAN_MSR_INAK));  //Wait for CAN to enter initialization mode
  
	CAN2->MCR &= ~CAN_MCR_TXFP;           //Priority driven by identifier
	
  CAN2->BTR &= ~(0x037F0000);           //Reset default time segments to zero
  CAN2->BTR |= (10)<<16;               //Set time segment 1 quanta
  CAN2->BTR |= (7)<<20;               //Set time segment 2 quanta
  CAN2->BTR |= 0x4;                     //Set quanta for 10MHz tq on 50MHz APB1 with above segments
	CAN2->BTR |= CAN_BTR_SJW;
	
  
  CAN2->MCR &= ~CAN_MCR_TTCM;
  CAN2->MCR |= CAN_MCR_ABOM; //Allow for automatic recovery from a bus-off fault
  CAN2->MCR &= ~CAN_MCR_AWUM;
  CAN2->MCR |= CAN_MCR_NART; //Do not retry
  CAN2->MCR &= ~CAN_MCR_RFLM;
  CAN2->MCR &= ~CAN_MCR_TXFP;
  
  CAN2->MCR &= ~CAN_MCR_INRQ;           //Exit initialization mode
  
  while (CAN2->MSR & CAN_MSR_INAK);     //Wait for initialization mode to close

	
	
	
	//Setup interrupts
	NVIC_SetPriorityGrouping(0);
	
	NVIC_SetPriority(USART2_IRQn, 2); //USART interrupt
	NVIC_EnableIRQ(USART2_IRQn);

	uint32_t rdhr_buffer;
	uint8_t button_byte_contents;
	while(1) {
		//Send stuff from the sway bar to the Jeep unchanged
		//This sets the status of the LED on the dashboard
		//Remarkably there is no logic in the Jeep's main computer to sanity check against speed! So lucky!
		runMirror(CAN2, CAN1);

		
		//When the Jeep sends commands, if it fits any of these three commands then intercept the message and re-transmit
		//For all other messages, just discard them as they are unnecessary to operate the sway bar. It only needs these three.
		if (CAN1->RF0R & 0x3) { //Message from the Jeep
			switch ((uint32_t)CAN1->sFIFOMailBox[0].RIR) {
				case ((uint32_t)0x325 << 21):
					//This one contains the sway bar button and some other things.
					//Download contents of byte 6 of the message
					//Then transmit length 7 message 0xFDE01EE1C00F0C
					//Masked with 0xFF00FFFFFFFFFF
					//And filling in the missing bytes from the downloaded contents of byte 6
					button_byte_contents = (((CAN1->sFIFOMailBox[0].RDHR) >> 8) & 0xFF); //6th CAN packet byte
					rdhr_buffer = 0xFDE01E;
					rdhr_buffer &= 0xFFFF00FF;
					rdhr_buffer |= (((uint32_t)button_byte_contents) << 8);
					TXCanMessage(CAN2, CAN1->sFIFOMailBox[0].RIR, 7, 0xE1C00F0C, rdhr_buffer);
					break;
				case ((uint32_t)0x428 << 21):
					//This one, just ignore the contents coming from the Jeep
					//And transmit length 7 message 0x00007E00001001
					TXCanMessage(CAN2, CAN1->sFIFOMailBox[0].RIR, 7, 0x00001001, 0x00007E);
					break;
				case ((uint32_t)0x215 << 21):
					//This one, just ignore the contents coming from the Jeep
					//And transmit length 7 message 0x8F000000000000
					TXCanMessage(CAN2, CAN1->sFIFOMailBox[0].RIR, 7, 0x00000000, 0x8F0000);
					break;
				default:
					break;
			}
			
			CAN1->RF0R |= CAN_RF1R_RFOM1; //Release Jeep's mailbox to next FIFO entry
		}
	}

}
	


void TXCanMessage(CAN_TypeDef * target, uint32_t address, uint8_t len, uint32_t RDLR, uint32_t RDHR) {
	uint8_t target_mailbox = 0;

	//Find an open transmit mailbox
	if (target->TSR & CAN_TSR_TME0) {
		target_mailbox = 0;
	} else if (target->TSR & CAN_TSR_TME1) {
		target_mailbox = 1;
	} else if (target->TSR & CAN_TSR_TME2) {
		target_mailbox = 2;
	} else {
		return; //There are no mailboxes available, have to purge message
	}
	
	//Set the CAN address
	target->sTxMailBox[target_mailbox].TIR = (address & 0xFFFFFFF8);

	//Set the length
	target->sTxMailBox[target_mailbox].TDTR = len & 0xF;
	
	//Set the data
	target->sTxMailBox[target_mailbox].TDLR = RDLR;
	target->sTxMailBox[target_mailbox].TDHR = RDHR;
			
	//Transmit
	target->sTxMailBox[target_mailbox].TIR |= CAN_TI0R_TXRQ;
}



void runMirror(CAN_TypeDef * source, CAN_TypeDef * target) {
	uint8_t target_mailbox = 0;
	if (source->RF0R & 0x3) {
		//There's something being received

		//Find an open transmit mailbox
		if (target->TSR & CAN_TSR_TME0) {
			target_mailbox = 0;
		} else if (target->TSR & CAN_TSR_TME1) {
			target_mailbox = 1;
		} else if (target->TSR & CAN_TSR_TME2) {
			target_mailbox = 2;
		} else {
			return; //There are no mailboxes available, have to purge message
		}

		//Set target length
		if (source->sFIFOMailBox[0].RIR & CAN_RI0R_IDE) {
			//Extended ID
			target->sTxMailBox[target_mailbox].TIR = (source->sFIFOMailBox[0].RIR & 0xFFFFFFF8) | CAN_TI0R_IDE;
		} else {
			//Standard ID
			target->sTxMailBox[target_mailbox].TIR = (source->sFIFOMailBox[0].RIR & 0xFFFFFFF8);
		}
		
		//Set the length
		target->sTxMailBox[target_mailbox].TDTR = source->sFIFOMailBox[0].RDTR & 0xF;
		
		//Set the data
		target->sTxMailBox[target_mailbox].TDLR = source->sFIFOMailBox[0].RDLR;
		target->sTxMailBox[target_mailbox].TDHR = source->sFIFOMailBox[0].RDHR;
		
		
		//Transmit
		target->sTxMailBox[target_mailbox].TIR |= CAN_TI0R_TXRQ;

	
		source->RF0R |= CAN_RF1R_RFOM1; //Release mailbox output to next FIFO entry
	}
}


