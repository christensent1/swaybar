#ifndef MAIN_h
#define MAIN_h

#include "stm32f412rx.h"

#define GPIO_INPUT (0x0)
#define GPIO_OUTPUT (0x1)
#define GPIO_AF (0x2)
#define GPIO_ANALOG (0x3)

#define GPIO_PUSH_PULL (0x0)
#define GPIO_OPEN_DRAIN (0x1)

#define GPIO_SPD_LOW (0x0)
#define GPIO_SPD_MEDIUM (0x1)
#define GPIO_SPD_HIGH (0x2)
#define GPIO_SPD_VERY_HIGH (0x3)

#define GPIO_PUPD_NO (0x0)
#define GPIO_PUPD_UP (0x1)
#define GPIO_PUPD_DOWN (0x2)

#define GPIO_INIT_VAL_LOW (0x0)
#define GPIO_INIT_VAL_HIGH (0x1)

void TXCanMessage(CAN_TypeDef * target, uint32_t address, uint8_t len, uint32_t RDLR, uint32_t RDHR);
void runMirror(CAN_TypeDef * source, CAN_TypeDef * target);
uint8_t toleratedContains(uint32_t address);
#endif

